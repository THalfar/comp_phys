Ensimmäisen viikon harjoitukset.

Sisältää numeerista derivointia ja integrointia ja näiden virhearviointeja.

num_calculus.py :

Sisältää ensimmäisen ja toisen kertaluvun derivaattojen laskemisen.
Sisältää riemannin summan, trapedoizin, simpsonin ja monte carlo menetelmän
integroimiseen.

convergence_check.py :

Sisältää nopeasti tehdyn virhetarkastelun eri dx kooille derivaatoille
ja trapetsoidille lasketulle integraalille.